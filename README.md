# IW System Status Frontend

aka "doc"

*A webpage for consuming and displaying the IW System Status API. Displays internal system statuses*



# Development
* `yarn install` (duh)
* `npm start`, or open webpack.config.js and make changes, (most likely only to config.output.publicPath)


# Deploying
* you'll want to set config.output.publishPath in webpack.config.js to whatever server/ip will be serving the compiled page.
* npm run build will build and compile the site.
* upload the compiled dist folder to your server.