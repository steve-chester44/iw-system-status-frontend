import "./home.scss";

class HomeViewController {

    constructor() {
        this.chevrons = {
            open: 'img/chevron-up.svg',
            closed: 'img/chevron-down.svg'
        }
    }

    $postLink() {
        this.groupServices();
    }
    groupServices() {
        this.status.$promise.then(status => {
            this.groups = status.services.reduce((r, a) => {
                r[a.group] = r[a.group] || [];
                r[a.group].push(a);
                return r;
            }, Object.create(null));
        });
    }

    toggleGroup(key) {
        this.groups[key].hidden = !this.groups[key].hidden;
    }
}

export const homeView = {
    controller: HomeViewController,
    bindings: {
    	status: '<'
    },
    template: `
<div class="container">
    <div class="card">
        <div class="group-wrapper" ng-repeat="(gkey, group) in $ctrl.groups">
            <div class="group-header">
                <h2>{{ gkey }}</h2>
                <img ng-src="{{ group.hidden ? $ctrl.chevrons.open : $ctrl.chevrons.closed }}" ng-click="$ctrl.toggleGroup(gkey)">
            </div>
            <div class="group-items" ng-hide="group.hidden">
                <service-item ng-repeat="(key, service) in group" class="service" service="service" key="key"></service-item>
            </div>
        </div>
    </div>
</div>   
  `
};
