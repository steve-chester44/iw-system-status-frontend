export const homeState = {
	parent: "app",
	name: "home",
	url: "/",
	views: {
		"main": "homeView"
	}
};
