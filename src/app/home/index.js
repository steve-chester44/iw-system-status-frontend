import { loadNg1Module, ngmodule } from "../bootstrap/ngmodule";
import { homeState } from "./home.states";
import { homeView } from "./home.component";

import { serviceItem } from "./components/serviceItem";

const mainAppModule = {
	components: { homeView, serviceItem },
	states: [homeState]
};

loadNg1Module(ngmodule, mainAppModule);
