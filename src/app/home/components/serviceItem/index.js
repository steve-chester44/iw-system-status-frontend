import "./serviceItem.scss";

class ServiceItemController {
	$onInit() {
		let statuses = [
			{
				status: "up",
				icon: "img/check-circle.svg",
				message: "Service is operating normally"
			},
			{
				status: "down",
				icon: "img/x-circle.svg",
				message: "Service is down"
			},
			{
				status: "unknown",
				icon: "img/alert-circle.svg",
				message: "Service status is unknown"
			}
		];

		this.status = statuses.find(si => si.status === this.service.status);

	}
}

export const serviceItem = {
	controller: ServiceItemController,
	bindings: {
		key: "<",
		service: "="
	},
	template: `
<div class="left">
	<img ng-src="{{ $ctrl.status.icon }}">
	<h3>{{ $ctrl.service.label }}</h3>
</div>
<div class="right">
	<span>{{ $ctrl.status.message }}</span>
</div>
`
};
