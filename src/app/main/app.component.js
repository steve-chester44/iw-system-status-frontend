import './app.scss';

class AppController {

	$postLink () {

		this.status.$promise.then(status => {

			let last = Math.round(this.status.lastUpdate);
			let now = Math.round(new Date() / 1000);

			const lastUpdated = () => {
				let seconds = now - last;

				if (seconds < 59) {
					return `Last updated ${seconds} seconds ago.`;
				}

				let minsAgo = (seconds) / 60;

				return `Last updated ${minsAgo} minute${minsAgo <= 1 || 's' } ago.`;
			};

			this.lastUpdated = lastUpdated();
		});
	}

}

export const app = {
    controller: AppController,
    bindings: {
    	status: '<'
    },
    template:`
<header class="hero">
	<span class="last-updated">{{ $ctrl.lastUpdated }}</span>
  	<div class="hero-inner">
        <h1>IW System Status</h1>
	</div>
</header>
<div class="main-container">
	<div class="content-container" ui-view="main"></div>
</div>
`
};
