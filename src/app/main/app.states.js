export const appState = {
  name: 'app',
  redirectTo: 'generator',
  resolve: {
		"status": ['ApiService', (ApiService) => ApiService.status()]
	},
  component: 'app'
};

