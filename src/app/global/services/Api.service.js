export class ApiService {
  constructor($resource) {
    "ngInject";

    this.endPoint = API_ENDPOINT;

    return $resource(
      this.endPoint, {},
      {
        status: {
          url: this.endPoint + "/status",
          method: "GET"
        }
      }
    );
  }
}
