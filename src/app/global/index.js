import {loadNg1Module, ngmodule} from '../bootstrap/ngmodule';

import { ApiService } from "./services/Api.service";

const globalAppModule = {
  directives: {},
  services: { ApiService },
  runBlocks: []
};

loadNg1Module(ngmodule, globalAppModule);
